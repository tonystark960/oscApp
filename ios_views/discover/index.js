/**
* 发现
*/

'use strict'

import Util from "../common/Util"
import Api from "../common/Api"

import Shake from "./shake"
import FindUser from "./find_user"
import Scan from "./scan"

var React = require('react-native');
var {
  StyleSheet,
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  Image,
}=React;

export default class Discover extends React.Component{
  constructor(props){
    super(props)
  }

  _loadPage(component,title){
    this.props.navigator.push({
      component:component,
      title: title
    })
  }

  render(){
    return (
      <ScrollView style={styles.container}>


        <View style={[styles.item,{marginTop:10}]}>
          <TouchableOpacity>
            <View style={styles.cell_1}>
              <Image
                style={styles.img}
                source={require('image!discover-events')}>
              </Image>
              <Text style={styles.title}>好友圈</Text>
            </View>
          </TouchableOpacity>
        </View>

        <View style={[styles.item,{marginTop:30}]}>
          <TouchableOpacity
            onPress={this._loadPage.bind(this,<FindUser/>,'找人')}>
            <View style={styles.cell_1}>
              <Image
                style={styles.img}
                source={require('image!discover-search')}>
              </Image>
              <Text style={styles.title}>找人</Text>
            </View>
          </TouchableOpacity>
        </View>

        <View style={styles.item}>
          <TouchableOpacity >
            <View style={styles.cell_1}>
              <Image
                style={styles.img}
                source={require('image!discover-activities')}>
              </Image>
              <Text style={styles.title}>活动</Text>
            </View>
          </TouchableOpacity>
        </View>

        <View style={[styles.item,{marginTop:30}]}>
          <TouchableOpacity
            onPress={this._loadPage.bind(this,<Scan/>,'扫一扫')}>
            <View style={styles.cell_1}>
              <Image
                style={styles.img}
                source={require('image!discover-scan')}>
              </Image>
              <Text style={styles.title}>扫一扫</Text>
            </View>
          </TouchableOpacity>
        </View>

        <View style={styles.item}>
          <TouchableOpacity
            onPress={this._loadPage.bind(this,<Shake/>,'摇一摇')}>
            <View style={styles.cell_1}>
              <Image
                style={styles.img}
                source={require('image!discover-shake')}>
              </Image>
              <Text style={styles.title}>摇一摇</Text>
            </View>
          </TouchableOpacity>
        </View>

      </ScrollView>
    );
  }
}

var styles = StyleSheet.create({
  container:{
    backgroundColor:'#eeedf1'
  },
  item:{
    flexDirection:'column',
    flex:1,
    height:40,
    justifyContent: 'center',
    borderTopWidth: Util.pixel,
    borderTopColor: '#ddd',
    backgroundColor:'#fff',
  },
  title:{
    marginLeft:15,
    marginTop:5,
  },
  img:{
    marginLeft:10
  },
  cell_1:{
    flexDirection:'row'
  },
});
