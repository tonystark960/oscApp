'use strict'

// htmlparser will check runtime
global.__filename = global.__filename ? global.__filename : '';
global.__dirname = global.__dirname ? global.__dirname : '';
var htmlparser = require("htmlparser");

var React = require('react-native')

var {
  View,
  Text,
  StyleSheet,
  AlertIOS,
} = React ;


export default class HtmlParserDemo extends React.Component{
  constructor(props){
    super(props)

  }
  componentDidMount(){
    let url='http://www.oschina.net/action/api/news_list';
    fetch(url)
      .then((response) => response.text())
      .then((responseText) => {
        var handler =  new htmlparser.DefaultHandler(()=>{},{ verbose: false, ignoreWhitespace: true ,enforceEmptyTags:false});
        var parser =  new htmlparser.Parser(handler);
        parser.parseComplete(responseText);
        //console.log(handler.dom);
        console.log(htmlparser.DomUtils.getElementsByTagName("news", handler.dom));
        var news=htmlparser.DomUtils.getElementsByTagName("news", handler.dom);

      })
      .catch(function(err){
        AlertIOS.alert('出错了',err);
      });
  }


  render(){
    return (
      <View>
        <Text>test</Text>
      </View>
    );
  }
}
