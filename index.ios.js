/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

import NavigatorWrapper from "./ios_views/common/NavigatorWrapper"
import News from "./ios_views/news/news_list"
import TabBarNavigator from 'react-native-tabbar-navigator';
import MeApp from "./ios_views/user"
import Discover from "./ios_views/discover"
import Tweet from "./ios_views/tweet"

import ListViewDemo from "./ios_views/example/ListView_Demo"
import HtmlParserDemo from "./ios_views/example/htmlparser_demo"
import ScrollViewDemo from "./ios_views/example/ScrollView_Demo"

var React = require('react-native');
var {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TabBarIOS,
  NavigatorIOS
} = React;

var oscApp = React.createClass({
  getInitialState:function(){
    return {
      selectedTab:0,
    }
  },
  _renderContent:function(contentTab,color){
    return (
      <View style={styles.contentTab}>
        <Text style={[styles.contentText,{color:color}]}>{contentTab}</Text>
      </View>
    )
  },
  // _addNavigator: function(component, title){
  //   return <NavigatorIOS
  //     style={{flex:1}}
  //     barTintColor='#4ead7d'
  //     titleTextColor="#FFFFFF"
  //     tintColor="#FFFFFF"
  //     translucent={false}
  //     initialRoute={{
  //         component: component,
  //         title: title,
  //         passProps:{
  //         }
  //       }}
  //     />;
  // },
  render: function() {
    return (
      <TabBarNavigator
        navTintColor='#ffffff'
        navBarTintColor='#4ead7d'
        tabTintColor='#4ead7d'
        onChange={(index)=>console.log(`selected index ${index}`)}>
        <TabBarNavigator.Item
          title="综合"
          icon={this.state.selectedTab === "综合"?require('image!tabbar-news-selected'):require('image!tabbar-news')}
          defaultTab>
          <News />
        </TabBarNavigator.Item>

        <TabBarNavigator.Item
          title="动弹"
          icon={this.state.selectedTab==="动弹"?require('image!tabbar-tweet-selected'):require('image!tabbar-tweet')}
          >
          <Tweet/>
        </TabBarNavigator.Item>

        <TabBarNavigator.Item
          title="发现"
          icon={this.state.selectedTab==="发现"?require('image!tabbar-discover-selected'):require('image!tabbar-discover')}
          >
          <Discover/>
        </TabBarNavigator.Item>

        <TabBarNavigator.Item
          title="我"
          icon={this.state.selectedTab==="我"?require('image!tabbar-me-selected'):require('image!tabbar-me')}
          >
          <MeApp/>
        </TabBarNavigator.Item>
{/*
        <TabBarNavigator.Item
          title="demo"
          icon={this.state.selectedTab==="demo"?require('image!tabbar-me-selected'):require('image!tabbar-me')}
          >
          <ScrollViewDemo/>
        </TabBarNavigator.Item>*/}
      </TabBarNavigator>

      // <TabBarIOS
      //   translucent={true}>
      //     <TabBarIOS.Item
      //         title="综合"
      //         icon={this.state.selectedTab === "综合"?require('image!tabbar-news-selected'):require('image!tabbar-news')}
      //         selected={this.state.selectedTab === "综合"}
      //         onPress={()=>{
      //             this.setState({
      //                 selectedTab:"综合"
      //             })
      //         }}>
      //         {this._addNavigator(News,"综合")}
      //     </TabBarIOS.Item>
      //
      //     <TabBarIOS.Item
      //         title="动弹"
      //         icon={this.state.selectedTab==="动弹"?require('image!tabbar-tweet-selected'):require('image!tabbar-tweet')}
      //         selected={this.state.selectedTab === "动弹"}
      //         onPress={()=>{
      //             this.setState({
      //                 selectedTab:"动弹"
      //             })
      //         }}>
      //         {this._renderContent("动弹","#eb3219")}
      //     </TabBarIOS.Item>
      //
      //     <TabBarIOS.Item
      //         title="发现"
      //         icon={this.state.selectedTab==="发现"?require('image!tabbar-discover-selected'):require('image!tabbar-discover')}
      //         selected={this.state.selectedTab === "发现"}
      //         onPress={()=>{
      //             this.setState({
      //                 selectedTab:"发现"
      //             })
      //         }}>
      //         {this._renderContent("发现","#eb3219")}
      //     </TabBarIOS.Item>
      //
      //     <TabBarIOS.Item
      //         title="我的"
      //         icon={this.state.selectedTab==="我的"?require('image!tabbar-me-selected'):require('image!tabbar-me')}
      //         selected={this.state.selectedTab === "我的"}
      //         onPress={()=>{
      //             this.setState({
      //                 selectedTab:"我的"
      //             })
      //         }}>
      //         {this._renderContent("我的","#eb3219")}
      //     </TabBarIOS.Item>
      //
      // </TabBarIOS>
    );
  }
});

var styles = StyleSheet.create({
  contentTab:{
    flex:1,
    alignItems:'center',//水平居中
    justifyContent:'center'//垂直居中

  },
  contentText:{

  },
});

AppRegistry.registerComponent('oscApp', () => oscApp);
