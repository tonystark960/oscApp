# React Native Demo
仿开源中国app，react native版实现，目前只实现了部分功能；
由于开源中国openApi返回的为xml数据，所以增加了nodejs中间层，用于调用openapi接口，然后转换为json数据。

## 如何运行

1. 中间层是用的nodejs的express包，进入根目录下的server目录，执行`npm install && npm start`
2. 在根目录下执行 `npm install && npm start`
3. 最后在`Xcode`中点击`run` 运行 或者按 `command + R`

## 效果如下图所示：

![](pic/综合.png)
![](pic/动弹.png)

![](pic/发现.png)
![](pic/我.png)
