/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

import NavigatorWrapper from "./ios_views/common/NavigatorWrapper"
import News from "./ios_views/news/news_list"
import Home from "./home"


var React = require('react-native');
var {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TabBarIOS,
  Navigator,
  NavigatorIOS
} = React;

var oscApp = React.createClass({
  getInitialState:function(){
    return {
      component:Home,
    }
  },
  _renderContent:function(contentTab,color){
    return (
      <View style={styles.contentTab}>
        <Text style={[styles.contentText,{color:color}]}>{contentTab}</Text>
      </View>
    )
  },
  render: function() {
    return (
      // <Navigator
      //   initialRoute={{name: '', component: this.state.component, index:0}}
      //   configureScene={()=>{return Navigator.SceneConfigs.FloatFromBottom;}}
      //   renderScene={(route, navigator) => {
      //     const Component = route.component;
      //     return (
      //       <View style={{flex: 1}}>
      //         <Component navigator={navigator} route={route} {...route.passProps}/>
      //       </View>
      //     );
      //   }}
      //   />

      <NavigatorIOS
        style={styles.container}
        initialRoute={{
          component: this.state.component,
          title: '综合',
          passProps: {
          },
        }}
        tintColor="#FFFFFF"
        barTintColor="#4ead7d"
        titleTextColor="#FFFFFF"
        translucent={false}
      />

    );
  }
});

var styles = StyleSheet.create({
  container:{
    flex:1,
    elevation:1
  },
  contentTab:{
    flex:1,
    alignItems:'center',//水平居中
    justifyContent:'center'//垂直居中

  },
  contentText:{

  },
});

AppRegistry.registerComponent('oscApp', () => oscApp);
